;;(defun abs (a) (if (< a 0) (* a (-1)) (a)))

(defun f (x) (+ (* (* (* x x) 4) -1) 1)) 

(defun bis (e f a b) (if (or (< (abs (- a b)) e) (= (funcall f (/ (+ a b) 2)) 0))
	(/ (+ a b) 2)
 	
 	(if (< (* (funcall f (/ (+ a b) 2)) (funcall f a)) 0)
 		(bis e f a (/ (+ a b) 2)) 
 		(bis e f (/ (+ a b) 2) b))))	
