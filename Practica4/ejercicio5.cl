(defun lengt (n) (if (eq nil n) 0 (+ 1 (lengt (cdr n))))
;; Para llamar a la función en el intérprete:
;; (lengt (list 1 2 3))
;; (lengt '(1 2 3))
;; Para cargar (load "ejercicio5.cl")
