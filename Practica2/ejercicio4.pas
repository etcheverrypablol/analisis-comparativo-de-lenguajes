// 4. Muestre un ejemplo de un programa Pascal que compile
// pero que tenga un error de tipos en ejecución.

// Pascal tiene un chequeo de tipos de variables estático
program errorTipoEnEjecucion;

var
	i:integer;

begin
	writeln('Ingrese un numero entero:');
	readln(i);
end.

// Esto no es un ejemplo de erro de tipo en ejecucion
{ begin
	i:=10;
	while i>-2 do
	begin
		writeln(sqrt(i));
		i:=i-1
	end;
end.
}