// 3. Muestre un ejemplo de programa en Haskell y en Pascal que no
// compile por un error de tipos aún cuando nunca se produciría en
// ejecución.

program error;
var
	num:integer;
begin
	num:=1;
	if false then
		num:='Esto genera el error en compilacion. Sin embargo nunca 
		se generaria en tiempo de ejecucion';
end.