// 5. Realice un experimento para determinar qué sistema de equivalencia
// de tipos usa Pascal. Justifique su respuesta.

// TIPOS DE EQUIVALENCIAS QUE HAY:

// Equivalencia Estructural: Requiere que los dos tipos tengan la misma
// estructura

// Equivalencia por nombre: Las dos expresiones de tipo deben tener el 
// mismo nombre

program EquivalenciaPorNombre;

type
	registro = record
			x:integer;
			y:integer;
		   end;
	registro1 = record
			x:integer;
			y:integer;
		   end;

	arreglo= array[1..10] of integer;
	arreglo1= array[1..10] of integer;

var
	r1:registro;
	r2:registro;
	r3:registro1;

	a1:arreglo;
	a2:arreglo;
	a3:arreglo1;
begin

	r1:=r2; // Esto se puede hacer
	//r1:=r3; // Esto no se puede hacer

	a1:=a2;	// Esto se puede hacer
	a1:=a3;	// Esto se puede hacer
end.

// Parece ser que en Pascal es por nombre para tipos no básicos 
// y por estructura para tipos básicos.
// Pero Sucede que para registros y arreglos que son tipos no básicos, se comporta distinto.
// Por nombre para registros, y por estructura para arreglos