//Ejercicio: Dar un ejemplo en un lenguaje imperativo de una expresión 
//que no tenga transparencia referencial.

// La transparencia referencial es un término utilizado en la 
// programación funcional que se refiere a la propiedad por la cual
// "en un programa, una expresión E del lenguaje puede ser sustituida
// por otra de igual valor V, resultando en un programa cuya semántica
// no va a diferir de la del original".

program noTranspReferencial;

var 
	global:integer;

function succ(x:integer):integer;
begin
	x:=x+global;
	global:=global+1;
end;

begin
	global:=1;
	writeln('El sucesor de 1 es: ',succ(1));
	writeln('El sucesor de 1 es: ',succ(1));
end.
