#include <stdio.h>

int main(int argc, char const *argv[])
{
	int num;
	int sumDiv=0;

	printf("Ingrese un numero a saber si es perfecto: ");
	scanf("%i",&num);	

	for (int i = 1; i <= (num/2); i++)
	{
		if(num % i == 0){
			sumDiv=sumDiv+i;
		}
	}
	if(num==sumDiv){
		printf("El numero es perfecto.\n");
	}else{
		printf("El numero NO es perfecto.\n");
	}
	return 0;
}
