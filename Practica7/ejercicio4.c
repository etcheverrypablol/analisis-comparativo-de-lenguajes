#include <stdio.h>
#include <stdlib.h>

int bis(int e, int (*f) (int), int a, int b){
	if( e > abs(a-b) || (f((a+b)/2) == 0)){
		return (a+b)/2;
	}
	if((f((a+b)/2)*(f(a)))<0){
		//return 2;
		bis(e,(*f),a,((a+b)/2));
	}else{
		//return 3;
		bis(e,f,((a+b)/2),b);
	}
}

int cuad(int x){
	return -(x*x-1);
}

int main(int argc, char const *argv[])
{
	printf("Resultado %i",bis(0,*cuad,-1,1));
	return 0;
}

