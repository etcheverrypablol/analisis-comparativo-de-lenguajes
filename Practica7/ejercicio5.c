#include <stdio.h>
#define COL 10
#define FIL 10

void cargaArrayBi(int arrayBi[FIL][COL]);
void mostrarArrayBi(int arrayBi[FIL][COL]);
int * filaQueMasSuma(int arrayBi[FIL][COL]);
void mostrarDireccionesFilas(int arrayBi[FIL][COL]);


int main(int argc, char const *argv[])
{

	int arrayBi [FIL][COL];
	cargaArrayBi(arrayBi);
	mostrarArrayBi(arrayBi);
	printf("DIRECCIONES DE LAS FILAS:\n");
	mostrarDireccionesFilas(arrayBi);
	printf("La fila que más suma es: %p\n",filaQueMasSuma(arrayBi));
	return 0;
}

int * filaQueMasSuma(int arrayBi[FIL][COL]){
	int * fila;
	int sumaMax=0;
	int sumaFila=0;
	// Tratamiento primera fila
	for (int j = 0; j < COL; ++j)
	{
		sumaMax=sumaMax+arrayBi[0][j];
	}
	fila=&arrayBi[0][0];
	// Tratamiento demás filas
	for (int i = 1; i < COL; ++i)
	{
		sumaFila=0;
		for (int j = 0; j < FIL; ++j)
		{
			sumaFila=sumaFila+arrayBi[i][j];
		}
		printf("Suma Fila %i: %i\n",i,sumaFila);
		if(sumaFila>sumaMax){
			sumaMax=sumaFila;
			fila=&arrayBi[i][0];
		}
	}
	return fila;
}

void cargaArrayBi(int arrayBi[FIL][COL]){
	
	for (int i = 0; i < FIL; ++i)
	{
		for (int j = 0; j < COL; ++j)
		{
			if(i==3){
				arrayBi[i][j]=2;
			}else{
				arrayBi[i][j]=1;
			}
		}
	}
}
void mostrarArrayBi(int arrayBi[FIL][COL]){
	for (int i = 0; i < FIL; ++i)
	{
		for (int j = 0; j < COL; ++j)
		{
			printf("%i ",arrayBi[i][j]);
		}
		printf("\n");
	}
}

void mostrarDireccionesFilas(int arrayBi[FIL][COL]){
	for (int i = 0; i < FIL; ++i)
	{
		printf("Fila %i-esima: %p\n",i,&arrayBi[i][0]);
	}
}