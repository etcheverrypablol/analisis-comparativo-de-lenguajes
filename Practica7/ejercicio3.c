#include <stdio.h>

void mostrarArray(int array[]);
void descomponer(int num, int descomp[]);

int main(int argc, char const *argv[])
{
	int num;
	int descomp[20];
	char arrayTest[20];
  	
  	printf("Ingrese el numero a saber si es capicua: ");
	scanf("%i",&num);
	descomponer(num, descomp);
	mostrarArray(descomp);
	return 0;
}


void descomponer(int num, int array[]){
	int nroCifras=1;
	int divisor=1;
	int aux=num;
	while((aux/10)>0){
		nroCifras++;
		aux=aux/10;
	}

	for (int i = 0; i < nroCifras; i++)
	{
		printf("%i\n",num%10);
		array[i]=num%10;
		num=num/10;
	}
	mostrarArray(array);
}

void mostrarArray(int array[]){
	printf("Mostrando el array: \n");
	int i=0;
	int cantElem=sizeof(array)/sizeof(int);
	printf("%i\n",array[i]);
	while(i<=cantElem){
		printf("%i\n",array[i]);
		i++;
	}
}
