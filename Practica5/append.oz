local Append in
      proc {Append A B ?C}
		case A of
		   nil then C=B
		[] X|As then Cs in
		   C=X|Cs
		   {Append As B Cs}
		end
	  end

   local X in
      {Append  [3] [123] X}
      {Browse X}
   end
end

%