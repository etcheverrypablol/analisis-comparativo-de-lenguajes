
%%%%%%%%%%%%%%%%%%%%%%%%%%% Chapter 5 del Apunte acl_book.pdf  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Lazy problem solving (Solve)

% This is the Solve operation, which returns a lazy list of solutions
% to a relational program.  The list is ordered according to a
% depth-first traversal.  Solve is written using the computation space
% operations of the Space module.


declare
fun {Solve Script}
   {SolStep {Space.new Script} nil}
end
fun {SolStep S Rest}
   case {Space.ask S}
   of failed then Rest
   [] succeeded then {Space.merge S}|Rest
   [] alternatives(N) then 
      {SolLoop S 1 N Rest}
   end
end
fun lazy {SolLoop S I N Rest}
   if I>N then Rest
   elseif I==N then
      {Space.commit S I}
      {SolStep S Rest}
   else Right C in
      Right={SolLoop S I+1 N Rest}
      C={Space.clone S}
      {Space.commit C I}
      {SolStep C Right}
   end
end
fun {SolveOne F}
  L={Solve F}
  in
    if L==nil then nil else [L.1] end
end
fun {SolveAll F}
    L={Solve F}
    proc {TouchAll L}
      if L==nil then skip else {TouchAll L.2} end
    end
  in
    {TouchAll L}
    L
end
