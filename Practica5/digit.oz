\insert 'solve.oz'
local Digit in
   fun {Digit}
      choice 0 [] 1 [] 2 [] 3 [] 4 [] 5 [] 6 [] 7 [] 8 [] 9 end
   end
   {Browse {SolveAll Digit}}

   local TwoDigit in
      fun {TwoDigit}
	 10*{Digit}+{Digit}
      end
   {Browse {SolveAll TwoDigit}}
   end

   local StrangeTwoDigit in
      fun {StrangeTwoDigit}
	 {Digit}+10*{Digit}
      end
      {Browse {SolveAll StrangeTwoDigit}}
   end
end
