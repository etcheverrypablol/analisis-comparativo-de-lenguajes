Program Example;

Var x:integer;

Function F(a:integer):integer;
begin
	// a=3 and x=2
	F := x*a
end;

Procedure P(y:integer);
var x:integer;
	z:boolean;
begin
	x := 1;
	z := (y mod 2 = 0);
	if z then begin
		x := F(y+1); // x=6 ya que el x al que se hace referencia
					 // es al que está en el ambiente de la
					 // declaracion de F, es decir, linea 3.
		writeln('x dentro de P (then): ',x);
	end
	else begin
		x := F(y);
		writeln('x dentro de P (else)',x);
	end;
end;

begin { main }
	x := 2;
	P(x);
	writeln('x: ',x); // El procedimiento usa pasaje de parametro por valor
end.
