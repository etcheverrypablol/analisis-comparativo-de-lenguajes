#include <stdio.h>
int x;

int F(int a){
	return x*a;
}

void P(int y){
	int x;
	int z;

	x=1;
	z=((y % 2) == 0);
	if(z){
		x=F(y+1);
		printf("x dentro de P (then): %i \n",x);
	}else{
		x=F(y);
		printf("x dentro de P (else): %i \n",x);
	}
}

int main(int argc, char const *argv[])
{
	x=2;
	P(x);
	printf("%i\n",x);
	return 0;
}
