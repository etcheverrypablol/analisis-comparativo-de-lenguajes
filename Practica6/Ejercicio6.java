public class Ejercicio6{
	int x;

	public int F(int a){
		return x*a;
	}

	public void P(int y){
		int x;
		boolean z;

		x=1;
		z=((y % 2) ==0);
		if(z){
			x=F(y+1);
		}else{
			x=F(y);
		}
	}

	public static void main(String[] args) {
		x=2;
		P(x);
	}
}